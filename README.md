## book-store-api

Skill test by SCB

# Required

* Test api by postman
```
{
	"info": {
		"_postman_id": "226cd946-719a-4307-a141-2bb55557de9d",
		"name": "book-store",
		"schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
	},
	"item": [
		{
			"name": "user-api",
			"item": [
				{
					"name": "getUser",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "http://localhost:8080/users",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"users"
							]
						}
					},
					"response": []
				},
				{
					"name": "createUser",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{ \r\n    \"username\": \"john.doe\", \r\n    \"password\": \"thisismysecret\",\r\n    \"date_of_birth\": \"15/01/1996\"\r\n}",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/users",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"users"
							]
						}
					},
					"response": []
				},
				{
					"name": "deleteUser",
					"request": {
						"method": "DELETE",
						"header": [],
						"url": {
							"raw": "http://localhost:8080/users",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"users"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "scb",
			"item": [
				{
					"name": "books",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://scb-test-book-publisher.herokuapp.com/books",
							"protocol": "https",
							"host": [
								"scb-test-book-publisher",
								"herokuapp",
								"com"
							],
							"path": [
								"books"
							]
						}
					},
					"response": []
				},
				{
					"name": "recommendBooks",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "https://scb-test-book-publisher.herokuapp.com/books/recommendation",
							"protocol": "https",
							"host": [
								"scb-test-book-publisher",
								"herokuapp",
								"com"
							],
							"path": [
								"books",
								"recommendation"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "orders",
			"item": [
				{
					"name": "/users/orders",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\"orders\": [1]} ",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/users/orders",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"users",
								"orders"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "auth",
			"item": [
				{
					"name": "/login",
					"request": {
						"method": "POST",
						"header": [],
						"body": {
							"mode": "raw",
							"raw": "{\r\n    \"username\":\"john.doe\", \r\n    \"password\": \"thisismysecret\"\r\n} \r\n",
							"options": {
								"raw": {
									"language": "json"
								}
							}
						},
						"url": {
							"raw": "http://localhost:8080/login",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"login"
							]
						}
					},
					"response": []
				}
			]
		},
		{
			"name": "books",
			"item": [
				{
					"name": "/books",
					"request": {
						"method": "GET",
						"header": [],
						"url": {
							"raw": "http://localhost:8080/books",
							"protocol": "http",
							"host": [
								"localhost"
							],
							"port": "8080",
							"path": [
								"books"
							]
						}
					},
					"response": []
				}
			]
		}
	]
}

```
* Run `` docker-compose up -d `` for create mongodb server
