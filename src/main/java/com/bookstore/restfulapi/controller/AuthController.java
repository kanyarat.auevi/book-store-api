package com.bookstore.restfulapi.controller;

import com.bookstore.restfulapi.model.CredentialModel;
import com.bookstore.restfulapi.model.UserModel;
import com.bookstore.restfulapi.services.AuthService;
import com.bookstore.restfulapi.validation.UserExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.session.data.mongo.config.annotation.web.http.EnableMongoHttpSession;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@RestController
@EnableMongoHttpSession
public class AuthController {

    @Autowired
    private AuthService authService;

    @RequestMapping(path = "/login", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void login(HttpSession session, @Valid @RequestBody CredentialModel credential) {
        String username = credential.getUsername();
        String password = credential.getPassword();
        if (username == null || username.equals("") || password == null || password.equals("")) {
            throw  new UserExceptionHandler();
        }
        UserModel user = this.authService.login(credential);
        session.setAttribute("userId", user.getId());
    }
}

