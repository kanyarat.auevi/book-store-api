package com.bookstore.restfulapi.controller;


import com.bookstore.restfulapi.model.BookModel;
import com.bookstore.restfulapi.services.BookService;
import com.bookstore.restfulapi.validation.BookExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(path = "/books", method = RequestMethod.GET)
    public Map<String, Object> getBooks() {
        BookModel[] books = this.bookService.getBooks();
        Map<String, Object> result = new HashMap<>();
        result.put("books", books);
        return result;
    }
}
