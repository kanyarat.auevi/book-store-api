package com.bookstore.restfulapi.controller;

import com.bookstore.restfulapi.model.OrderModel;
import com.bookstore.restfulapi.model.UserModel;
import com.bookstore.restfulapi.services.OrderService;
import com.bookstore.restfulapi.validation.LoginExceptionHandler;
import com.bookstore.restfulapi.validation.OrderExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.session.data.mongo.config.annotation.web.http.EnableMongoHttpSession;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
@EnableMongoHttpSession
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(path = "/users/orders", method = RequestMethod.POST)
    public Map<String, Double> orderBook(HttpSession session, @RequestBody OrderModel order) {
        int[] booksId = order.getBookIdList();
        if (booksId == null) {
            throw new OrderExceptionHandler();
        }
        String userId = (String)session.getAttribute("userId");
        if (userId == null || userId.equals("")) {
            throw new LoginExceptionHandler();
        }
        order.setUserId(userId);
        Map<String, Double> orderInfo = new HashMap<>();
        Double price = orderService.orderBook(order).getPrice();
        orderInfo.put("price", price);
        return orderInfo;
    }
}
