package com.bookstore.restfulapi.controller;

import com.bookstore.restfulapi.services.UserService;
import com.bookstore.restfulapi.validation.LoginExceptionHandler;
import com.bookstore.restfulapi.validation.UserExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.bookstore.restfulapi.model.UserModel;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Map;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(path = "/users", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(code = HttpStatus.OK)
    public void createUser(@Valid @RequestBody UserModel user) {
        this.userService.createUser(user);
    }

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    @ResponseStatus(code = HttpStatus.OK)
    public Map<String, Object> getUserInfo(HttpSession session) {
        String userId = (String)session.getAttribute("userId");
        if (userId == null || userId.equals("")) {
            throw new LoginExceptionHandler();
        }
        return this.userService.getUserInfo(userId);
    }

    @RequestMapping(path = "/users", method = RequestMethod.DELETE)
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteUser(HttpSession session) {
        String userId = (String)session.getAttribute("userId");
        if (userId == null || userId.equals("")) {
            throw new LoginExceptionHandler();
        }
        this.userService.deleteUser(userId);
        session.removeAttribute("userId");
    }
}
