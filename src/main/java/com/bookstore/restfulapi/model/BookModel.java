package com.bookstore.restfulapi.model;


import com.fasterxml.jackson.annotation.JsonProperty;

public class BookModel {
    private int id;
    private String bookName;
    private String authorName;
    private double price;

    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("book_name")
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @JsonProperty("author_name")
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return this.id;
    }

    public String getBookName() {
        return this.bookName;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public double getPrice() {
        return this.price;
    }
}
