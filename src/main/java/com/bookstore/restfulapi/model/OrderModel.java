package com.bookstore.restfulapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "orders")
public class OrderModel {
    @Id private String id;

    private String userId;
    private int[] bookIdList;
    private double price;

    public String getId() {
        return this.id;
    }

    public String getUserId() {
        return this.userId;
    }

    public int[] getBookIdList() {
        return this.bookIdList;
    }

    public double getPrice() {
        return this.price;
    }

    public void setId(String orderId) {
        this.id = orderId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("orders")
    public void setBookIdList(int[] bookIdList) {
        this.bookIdList = bookIdList;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
