package com.bookstore.restfulapi.repositories;


import com.bookstore.restfulapi.model.OrderModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Document(collection = "orders")
public interface OrderRepository extends MongoRepository<OrderModel, String> {
    OrderModel[] findByUserId(String userId);

    Long deleteByUserId(String userId);
}
