package com.bookstore.restfulapi.repositories;

import com.bookstore.restfulapi.model.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserModel,String> {

    UserModel findByUsername(String username);

    @Query("{ '_id': ?0}")
    UserModel findByUserId(String id);
}
