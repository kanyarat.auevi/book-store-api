package com.bookstore.restfulapi.services;

import com.bookstore.restfulapi.model.CredentialModel;
import com.bookstore.restfulapi.model.UserModel;
import com.bookstore.restfulapi.repositories.UserRepository;
import com.bookstore.restfulapi.validation.UserExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordService passwordService;

    public UserModel login(CredentialModel credential) {
        String username = credential.getUsername();
        UserModel userInfo = this.userRepository.findByUsername(username);
        if (userInfo == null) {
            throw new UserExceptionHandler();
        }

        String encodedPassword = userInfo.getPassword();
        String rawPassword = credential.getPassword();
        if (!this.passwordService.match(rawPassword, encodedPassword)) {
            throw new UserExceptionHandler();
        }
        return userInfo;
    }
}
