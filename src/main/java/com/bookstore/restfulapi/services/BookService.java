package com.bookstore.restfulapi.services;

import com.bookstore.restfulapi.model.BookModel;
import com.bookstore.restfulapi.validation.BookExceptionHandler;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class BookService {

    private final String booksApiUrl = "https://scb-test-book-publisher.herokuapp.com/books";
    private final String recommendedBooksApiUrl = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";

    public BookModel[] getBooks() {
        RestTemplate restTemplate = new RestTemplate();

        BookModel[] books = restTemplate.getForObject(this.booksApiUrl, BookModel[].class);
        BookModel[] recommendedBooks = this.getRecommendBooks();
        if (books == null || recommendedBooks == null) {
            throw new BookExceptionHandler();
        }

        int resultIndex = books.length;
        BookModel[] result = new BookModel[resultIndex];
        System.arraycopy(recommendedBooks, 0, result, 0, recommendedBooks.length);

        int countAddElement = recommendedBooks.length;
        for (BookModel book : books) {
            int id = book.getId();
            int count = 0;
            for (BookModel recommendBook : recommendedBooks) {
                if (id == recommendBook.getId()) break;
                count++;
            }

            if (count == recommendedBooks.length) {
                int lastIndex = countAddElement++;
                result[lastIndex] = book;
            }
        }
        return result;
    }

    public BookModel[] getRecommendBooks() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(this.recommendedBooksApiUrl, BookModel[].class);
    }
}
