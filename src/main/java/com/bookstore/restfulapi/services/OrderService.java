package com.bookstore.restfulapi.services;

import com.bookstore.restfulapi.model.BookModel;
import com.bookstore.restfulapi.model.OrderModel;
import com.bookstore.restfulapi.repositories.OrderRepository;
import com.bookstore.restfulapi.validation.OrderExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private BookService bookService;

    @Autowired
    private OrderRepository orderRepository;

    public OrderModel orderBook(OrderModel order) {
        BookModel[] books = this.bookService.getBooks();
        int[] booksId = order.getBookIdList();
        double sumPrice = 0;
        for (int id : booksId) {
            for (BookModel book: books ) {
                int bookId = book.getId();
                if (id == bookId) {
                    sumPrice += book.getPrice();
                    break;
                }
            }
        }
        try {
            OrderModel orderInfo = new OrderModel();
            orderInfo.setUserId(order.getUserId());
            orderInfo.setBookIdList(booksId);
            orderInfo.setPrice(sumPrice);
            this.orderRepository.insert(orderInfo);

            return orderInfo;

        } catch (Exception $e) {
            throw new OrderExceptionHandler();
        }
    }
}
