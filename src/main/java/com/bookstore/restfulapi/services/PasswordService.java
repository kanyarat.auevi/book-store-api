package com.bookstore.restfulapi.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
    private final String templatePassword = "SCB%sUSER";
    private final BCryptPasswordEncoder bcryptEncoder;

    public PasswordService() {
        this.bcryptEncoder = new BCryptPasswordEncoder();
    }

    public String encode(String rawPassword) {
        return this.bcryptEncoder.encode(String.format(templatePassword, rawPassword));
    }

    public boolean match(String rawPassword, String encodedPassword) {
        return this.bcryptEncoder.matches(String.format(templatePassword, rawPassword), encodedPassword);
    }
}
