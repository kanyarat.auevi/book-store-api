package com.bookstore.restfulapi.services;

import com.bookstore.restfulapi.model.OrderModel;
import com.bookstore.restfulapi.model.UserModel;
import com.bookstore.restfulapi.repositories.OrderRepository;
import com.bookstore.restfulapi.repositories.UserRepository;
import com.bookstore.restfulapi.validation.OrderExceptionHandler;
import com.bookstore.restfulapi.validation.UserExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PasswordService passwordService;

    public void createUser(UserModel user) {
        String username = user.getUsername();
        String rawPassword = user.getPassword();
        String dateOfBirth = user.getDateOfBirth();

        if (username == null || username.equals("") || rawPassword == null || rawPassword.equals("") || dateOfBirth == null || dateOfBirth.equals("")) {
            throw new UserExceptionHandler();
        }

        String[] nameSplit = username.split("\\.");
        if (nameSplit.length != 2) {
            throw new UserExceptionHandler();
        }
        String name = nameSplit[0];
        String surname = nameSplit[1];

        String encodedPassword = this.passwordService.encode(rawPassword);

        UserModel newUser = new UserModel();
        newUser.setUsername(username);
        newUser.setPassword(encodedPassword);
        newUser.setName(name);
        newUser.setSurname(surname);
        newUser.setDateOfBirth(dateOfBirth);

        this.userRepository.insert(newUser);
    }

    public Map<String, Object> getUserInfo(String userId) {
        UserModel user = this.userRepository.findByUserId(userId);
        if (user == null) {
            throw new OrderExceptionHandler();
        }
        OrderModel[] orders = this.orderRepository.findByUserId(userId);
        if (orders == null) {
            throw new OrderExceptionHandler();
        }

        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("name", user.getName());
        userInfo.put("surname", user.getSurname());
        userInfo.put("date_of_birth", user.getDateOfBirth());
        ArrayList<Integer> userOrders = new ArrayList<>();
        for (OrderModel order : orders) {
            for (int bookId : order.getBookIdList()) {
                userOrders.add(bookId);
            }
        }
        userInfo.put("books", userOrders);
        return userInfo;
    }

    public void deleteUser(String userId) {
        try {
            this.userRepository.deleteById(userId);
            this.orderRepository.deleteByUserId(userId);
        } catch (Exception e) {
            throw new UserExceptionHandler();
        }
    }
}
