package com.bookstore.restfulapi.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Can't get books from outer api")
public class BookExceptionHandler extends RuntimeException {
}
