package com.bookstore.restfulapi.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Please Login before")
public class LoginExceptionHandler extends RuntimeException {
}
