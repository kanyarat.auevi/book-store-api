package com.bookstore.restfulapi.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.FORBIDDEN, reason="Not complete detail of order")
public class OrderExceptionHandler extends RuntimeException{

}
