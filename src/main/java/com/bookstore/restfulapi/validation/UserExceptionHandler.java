package com.bookstore.restfulapi.validation;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN, reason="username or password is not correct")
public class UserExceptionHandler extends RuntimeException {
}
